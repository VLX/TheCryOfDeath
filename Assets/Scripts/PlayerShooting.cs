﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    // player shooting from his gun with a bullet

    public GameObject shot_ak, shot_spas, shot_sniper;
    public Transform shotspawn_ak, shotspawn_spas1, shotspawn_spas2, shotspawn_spas3, shotspawn_sniper, shotspawn_sniper2;
    public Camera snipe_cam, main_cam;

    public AudioClip bam,shotgun_sound,sniper_sound;

    private float nextfire;
    public float firerate = 0.25f;

    public GameObject ak, aks,spas, spass, sniper, sniper2;
    private bool fire;

    private int ak_act, spas_act, sniper_act;

    void Start()
    {
        fire = true;
        ak_act = PlayerPrefs.GetInt("ak_act");
        spas_act = PlayerPrefs.GetInt("spas_act");
        sniper_act = PlayerPrefs.GetInt("sniper_act");

        if (ak_act == 0 && spas_act == 0 && sniper_act == 0)
        {
            set_active_ak();
        }
        else
        {
            if(ak_act == 1)
                set_active_ak();
            if (spas_act == 1)
                set_active_spas();
            if (sniper_act == 1)
                set_active_sniper();
        }
        //if (ak.active == true) 
        //else fire = false;
    }

    void Update ()
    {
        if (Input.GetButtonDown("Fire2") && sniper.active && fire)
        {
            sniper2.active = true;
            sniper.active = false;
            main_cam.enabled = false;
            snipe_cam.enabled = true;
        }
        if (Input.GetButtonUp("Fire2") && sniper2.active && fire)
        {
            sniper2.active = false;
            sniper.active = true;
            main_cam.enabled = true;
            snipe_cam.enabled = false;
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (aks.active)
            {
                aks.active = false;
                ak.active = true;
            }
            if (spass.active)
            {
                spass.active = false;
                spas.active = true;
            }
        }
        if (Input.GetButton("Fire1") && Time.time > nextfire && fire)
        {
            if (ak.active)
            {
                ak.active = false;
                aks.active = true;
            }
            else
            {
                if (aks.active)
                {
                    nextfire = Time.time + firerate;
                    Instantiate(shot_ak, shotspawn_ak.position, shotspawn_ak.rotation);
                    GetComponent<AudioSource>().PlayOneShot(bam, 0.7F);               
                }
            }

            if (spas.active)
            {
                spas.active = false;
                spass.active = true;
            }
            else
            {
                if (spass.active)
                {
                    nextfire = Time.time + 5 * firerate;
                    Instantiate(shot_spas, shotspawn_spas1.position, shotspawn_spas1.rotation);
                    Instantiate(shot_spas, shotspawn_spas2.position, shotspawn_spas2.rotation);
                    Instantiate(shot_spas, shotspawn_spas3.position, shotspawn_spas3.rotation);
                    GetComponent<AudioSource>().PlayOneShot(shotgun_sound, 0.7F);
                }
            }

            if (sniper.active)
            {
                    nextfire = Time.time + 10 * firerate;              
                    Instantiate(shot_sniper, shotspawn_sniper.position, shotspawn_sniper.rotation);
                    GetComponent<AudioSource>().PlayOneShot(sniper_sound, 0.7F);
            }

            if (sniper2.active)
            {
                nextfire = Time.time + 10 * firerate;
                Instantiate(shot_sniper, shotspawn_sniper2.position, shotspawn_sniper2.rotation);
                GetComponent<AudioSource>().PlayOneShot(sniper_sound, 0.7F);
            }

        }
    }

    public void set_active_ak()
    {
        ak.active = true;
        spas.active = false;
        sniper.active = false;
        PlayerPrefs.SetInt("ak_act",1);
        PlayerPrefs.SetInt("spas_act",0);
        PlayerPrefs.SetInt("sniper_act",0);
    }

    public void set_active_spas()
    {
        ak.active = false;
        spas.active = true;
        sniper.active = false;
        PlayerPrefs.SetInt("ak_act", 0);
        PlayerPrefs.SetInt("spas_act", 1);
        PlayerPrefs.SetInt("sniper_act", 0);
        PlayerPrefs.SetInt("spas", 1);
    }

    public void set_active_sniper()
    {
        ak.active = false;
        spas.active = false;
        sniper.active = true;
        PlayerPrefs.SetInt("ak_act", 0);
        PlayerPrefs.SetInt("spas_act", 0);
        PlayerPrefs.SetInt("sniper_act", 1);
        PlayerPrefs.SetInt("sniper", 1);
    }

    public bool get_active_ak()
    {
        return aks.active;
    }

    public bool get_active_spas()
    {
        return spass.active;
    }

    public bool get_active_sniper()
    {
        return sniper.active;
    }

    public bool get_active_sniper2()
    {
        return sniper2.active;
    }

    public void set_fire(bool f)
    {
        fire = f;
    }
}
