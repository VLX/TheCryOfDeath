﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour
{
    public GameObject playerGO;
    private PlayerController pc;

    void Start()
    {
        pc = playerGO.GetComponent<PlayerController>();
    }

    void Update()
    {
        transform.Rotate(new Vector3(30, 0, 0) * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            pc.addHealth();
            this.gameObject.active = false;
        }

    }
}