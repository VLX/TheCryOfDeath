﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateMenu : MonoBehaviour {

    bool active = false;

    public GameObject playerGO;
    private Quests pc;
    private int quest;

    void Start()
    {
        pc = playerGO.GetComponent<Quests>();
    }

    void OnGUI() { 
   if(quest != 0) { 
        if (active)
        {
            Cursor.visible = true;
            GUILayout.BeginArea(new Rect(Screen.width * 4 / 10, Screen.height * 1 / 2f, Screen.width, Screen.height));
            if (GUILayout.Button("Yes", GUILayout.Width(200), GUILayout.Height(100)))
            {
                if(quest == 1)
                    pc.setq1();
                if (quest == 2)
                    pc.setq2();
                if (quest == 3)
                    pc.setq3();
                if (quest == 4)
                    pc.setq4();
                if (quest == 5)
                    pc.setq5();

                    active = false;
                Cursor.visible = false;
            }

            if (GUILayout.Button("No", GUILayout.Width(200), GUILayout.Height(100)))
            {
                active = false;
                Cursor.visible = false;
            }
            GUILayout.EndArea();
        }
        }
        else
        {
            active = false;
        }
    }
    

    public void activate(int qn)
    {
        quest = qn;
        active = true;
    }

    public void deactivate()
    {
        active = false;
    }
}
