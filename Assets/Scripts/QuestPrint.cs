﻿using UnityEngine;
using System.Collections;

public class QuestPrint : MonoBehaviour
{
    private string currentTalk = "";
    public GUIStyle fontsize;

    public GameObject playerGO;
    private Quests pc;

    void Start()
    {
        pc = playerGO.GetComponent<Quests>();
    }

    void OnGUI()
    {
            fontsize.fontSize = 30;
            fontsize.normal.textColor = Color.white;
            currentTalk = "Quests:\n";
        if (pc.getq1() == 1)
            currentTalk = currentTalk + "Kill 2 worms ("+pc.getcntq1()+"/2)\n";
        if (pc.getq2() == 1)
            currentTalk = currentTalk + "Kill 3 snails (" + pc.getcntq2() + "/3)\n";
        if (pc.getq3() == 1)
            currentTalk = currentTalk + "Kill 4 birds (" + pc.getcntq3() + "/4)\n";
        if (pc.getq4() == 1)
            currentTalk = currentTalk + "Kill 4 frogs (" + pc.getcntq4() + "/4)\n";
        if (pc.getq5() == 1)
            currentTalk = currentTalk + "Kill the Boss (" + pc.getcntq5() + "/1)\n";
        GUI.Label(new Rect(Screen.width * (1 / 100f), Screen.height * (28 / 100f), 0, 20), currentTalk, fontsize);
    }
}