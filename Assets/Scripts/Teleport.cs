﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

    public Transform target;
    public GameObject player;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            player.transform.position = new Vector3(target.position.x, target.position.y-2f, target.position.z + 12f);
    }
}
