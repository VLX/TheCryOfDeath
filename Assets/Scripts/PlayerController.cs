﻿using UnityEngine;

public class PlayerController : MonoBehaviour {
    // player movement, xp and health

    // movement
    public bool IsGrounded = false;
    private Rigidbody rb;
    public GameObject camera;
    public float speed;
    private Vector3 movement;

    // health
    public GameObject guibar;
    private GUIBarScript health;
    private float curhealth;

    // xp
    private int playerXP;
    private int level,lvltemp;
    public GameObject barxpGO;
    private GUIBarScript1 barxp;
    private float scale, scalexp;

    // cave open
    public GameObject cave;

    private float cx, cy, cz;

    private InGameMenu menu;

    void Start() {
        rb = GetComponent<Rigidbody>();
        health = guibar.GetComponent<GUIBarScript>();

        // loads the saved amount of xp of the player and sets the xp correctly
        if (PlayerPrefs.GetInt("xp") > 100) playerXP = PlayerPrefs.GetInt("xp");
        else playerXP = 101;

        // loads the saved amount of health of the player and refills it if the player was dead
        if (PlayerPrefs.GetFloat("health") > 0) curhealth = PlayerPrefs.GetFloat("health");
        else curhealth = 1;

        // converts the xp amount to level using Log because of the sequence of level's xp
        level = (int)(Mathf.Log(playerXP / 100f, 2) + 1);
        lvltemp = level;

        barxp = barxpGO.GetComponent<GUIBarScript1>();
        // the scale of current to the next level in percentage
        scale = Mathf.Pow(2, level)*100 - Mathf.Pow(2, level - 1)*100;
        scalexp = playerXP - Mathf.Pow(2, level - 1) * 100;
        barxp.SetNewValue(scalexp/scale);

        //Loading
        cx = PlayerPrefs.GetFloat("PlayerX");
        cy = PlayerPrefs.GetFloat("PlayerY");
        cz = PlayerPrefs.GetFloat("PlayerZ");

        if (cx != 0f && cy != 0f && cz != 0f)
        transform.position = new Vector3(cx, cy, cz);

        menu = GetComponent<InGameMenu>();
    }
    void FixedUpdate() {
        // move
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        movement = new Vector3(moveHorizontal * 2, 0, moveVertical * 2);

        // jump if it is on the ground
        if (Input.GetKeyDown("space") && IsGrounded)
            rb.AddForce(Vector3.up * 5, ForceMode.Impulse);

        // moves the player based on camera rotation
        if (!menu.get_active())
        {
            rb.AddForce(moveVertical * 2 * camera.transform.forward * speed);
            rb.AddForce(moveHorizontal * 2 * camera.transform.right * speed);
        }

        // sets the new values of cp and health if changes
        level = (int)(Mathf.Log(playerXP / 100f, 2) + 1);
        if (lvltemp < level)
        {
            lvltemp = level;
            health.SetNewValue(1);
        }
        else
        {
            health.SetNewValue(curhealth);
        }

        // set new scale if level up
        scale = Mathf.Pow(2, level) * 100 - Mathf.Pow(2, level - 1) * 100;
        scalexp = playerXP - Mathf.Pow(2, level - 1) * 100;
        barxp.SetNewValue(scalexp / scale);

        // player dies
        if (curhealth <= 0)
        {
            // devides the xp by 2 but if xp/2<101 then the amount will set to 101
            if(playerXP / 2 < 101){PlayerPrefs.SetInt("xp", 101);}
            else{PlayerPrefs.SetInt("xp", playerXP / 2);}

            // refills and save player's health
            PlayerPrefs.SetFloat("health", 1);

            // go to Retry scene menu
            Application.LoadLevel("Retry");
        }

        // opens cave for testing
        if (Input.GetKey("o"))
        opencave();
    }

    // called by stage king enemy if died to open the entrance of the next stage
    public void opencave(){
        // check if cave exists
        if(cave)
        cave.active = true;
        if (Input.GetKey("o"))
            playerXP = 5120000;
        curhealth = 1;
    }

    // called by enemy if it died and adds xp to player
    public void addXP(int xp)
    {
        playerXP += xp;
        PlayerPrefs.SetInt("xp", playerXP);
    }

    // used to make the damage calculation
    public int getLevel(){return level;}

    // called by enemy if enemy attacks player after damage calculation
    public void eraseHealth(float ch)
    {
        curhealth -= ch;
        PlayerPrefs.SetFloat("health", curhealth);
    }

    // called by health cross if player touch it
    public void addHealth()
    {
        curhealth = 1;
        PlayerPrefs.SetFloat("health", curhealth);
    }

    // if player hit by enemy's fire balls
    void OnTriggerEnter(Collider other){
        if(other.tag == "enemy bullet")
            curhealth -= 2.2f / level;
    }

    // used to check if player touch or not the ground in order to jump or not
    void OnCollisionStay(Collision collisionInfo){IsGrounded = true;}
    void OnCollisionExit(Collision collisionInfo){IsGrounded = false;}
}