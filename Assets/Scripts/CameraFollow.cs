﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public float turnSpeed = 4.0f;
    public Transform player;
    private Vector3 offset;

    public Camera cm;

    private bool rotate;

    void Start()
    {
        rotate = true;
        offset = new Vector3(17, 8, 7);
        Cursor.visible = false;
    }

    void LateUpdate()
    {
        // rotates the camera with mouse around player if any menu is not open
        if (rotate) { 
        offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * turnSpeed, Vector3.up) * offset;
        offset = Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * turnSpeed, Vector3.left) * offset;
        transform.position = player.position + offset;
        }

        transform.LookAt(player.position);


        // camera zoom in/out with limit
        if (rotate)
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel");

            cm.fieldOfView -= scroll * 10;
            if (cm.fieldOfView < 10)
                cm.fieldOfView = 10;
            if (cm.fieldOfView > 70)
                cm.fieldOfView = 70;
        }
    }

    public void set_rotate(bool r)
    {
        rotate = r;
    }
}