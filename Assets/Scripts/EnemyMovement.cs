﻿using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    // follow player
    public Transform player,enemy;
    UnityEngine.AI.NavMeshAgent nav;
    private float distance;

    // health
    public TextMesh healthText;
    private float health = 100;
    public GameObject playerGO, Dante;
    private PlayerController pc;
    private PlayerShooting shooting;

    public int enemyXP;
    public int enemyLevel;

    // unlock next stage
    public bool stageking;

    private bool hit = false;
    private bool enemy_follow = false;

    private Quests questController;

    Animator anim;

    public EnemyMovement [] follower;

    void Start ()
    {
        nav = GetComponent<UnityEngine.AI.NavMeshAgent> ();
        pc = playerGO.GetComponent<PlayerController>();
        questController = playerGO.GetComponent<Quests>();
        shooting = Dante.GetComponent<PlayerShooting>();
        anim = GetComponent<Animator>();
    }


    void Update ()
    {
        // calculate the distance between enemy and player
        distance = Mathf.Sqrt(
                   Mathf.Pow(enemy.position.x - player.position.x, 2) +
                   Mathf.Pow(enemy.position.z - player.position.z, 2));

        // 50 is a logical distance to make enemy follow the player or it follow him if hit by bullet
        if (distance < 50 || hit || enemy_follow)
        {
            if (hit)
            {
                for (int i = 0; i < follower.Length; i++)
                    follower[i].follow();
            }
            nav.SetDestination(player.position);
            if (anim)
            {
                anim.SetBool("isAttacking", true);
                anim.Play("attacking");
            }
        }
        else
        {
            if (anim)
            {
                anim.SetBool("isAttacking", false);
                anim.Play("Start");
            }
        }

    }
    void OnTriggerEnter(Collider other)
    {
        // if hit by bullet
        if (other.tag == "Bullet")
        {
            // erase enemy's health with type 6*player level/enemy level
            if(shooting.get_active_ak())
                health -= 6 * pc.getLevel() / enemyLevel;

            if (shooting.get_active_spas())
                health -= 20 * pc.getLevel() / enemyLevel;

            if (shooting.get_active_sniper())
                health -= 50 * pc.getLevel() / enemyLevel;

            if (shooting.get_active_sniper2())
                health -= 50 * pc.getLevel() / enemyLevel;

            // enemy dies
            if (health <= 0)
            {
                // when the stage king dies, the gate of next stage must open
                if (stageking){pc.opencave();}

                Destroy(this.gameObject);
                // adds the amount of xp points that player must recieve
                pc.addXP(enemyXP);

                if(questController.getq1() == 1 && (enemyLevel == 1 || enemyLevel == 2))
                {
                    questController.setcntq1();
                }

                if (questController.getq2() == 1 && (enemyLevel == 3 || enemyLevel == 4))
                {
                    questController.setcntq2();
                }

                if (questController.getq3() == 1 && (enemyLevel == 5 || enemyLevel == 6))
                {
                    questController.setcntq3();
                }

                if (questController.getq4() == 1 && (enemyLevel == 7 || enemyLevel == 8))
                {
                    questController.setcntq4();
                }
            }           

            // prints the new value after the enemy hit by bullet
            healthText.text = "" + health;

            // enables enemy to follow you
            hit = true;
        }

        // if enemy reach the player 
        if (other.gameObject.tag == "Player")
        {
            // erase player health with type enemylevel*0.22/playerlevel
            pc.eraseHealth(enemyLevel * 0.22f / pc.getLevel());
        }

    }

    public void follow()
    {
        enemy_follow = true;
    }

}