﻿using UnityEngine;
using System.Collections;

public class Buttons1 : MonoBehaviour {

    // Retry page buttons
    public Canvas canvas;

    void OnGUI()
    {
        Cursor.visible = true;

        canvas.worldCamera = Camera.main;
        GUI.skin.button.fontSize = 20;
        GUILayout.BeginArea(new Rect(Screen.width*2 / 7, Screen.height * 2 / 7, Screen.width, Screen.height));

        if (GUILayout.Button("Retry", GUILayout.Width(500), GUILayout.Height(100)))
        {
            Destroy(this);
            Application.LoadLevel("0");
        }

        if (GUILayout.Button("Quit", GUILayout.Width(500), GUILayout.Height(100)))
        {
            Destroy(this);
            Application.Quit();
        }
        GUILayout.EndArea();
    }
}
