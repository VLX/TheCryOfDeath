﻿using UnityEngine;

public class Talk : MonoBehaviour
{
    public string[] conversation;

    private int convoIndex = 0;
    private string currentTalk = "";

    public GUIStyle fontsize;

    private bool stay = false;

    public GameObject playerGO, Dante, camer,camer2;
    private PlayerShooting gun_controller;
    private Animations anim;
    private ActivateMenu me;
    private CameraFollow cam_controll, cam_controll2;
    public int quest;

    public Texture2D backk;

    void Awake()
    {
        me = playerGO.GetComponent<ActivateMenu>();
        gun_controller = Dante.GetComponent<PlayerShooting>();
        anim = Dante.GetComponent<Animations>();
        cam_controll = camer.GetComponent<CameraFollow>();
        cam_controll2 = camer2.GetComponent<CameraFollow>();
    }

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            stay = true;
            gun_controller.set_fire(false);
            anim.set_talk(true);
            cam_controll.set_rotate(false);
            cam_controll2.set_rotate(false);
        }        
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            currentTalk = "";
            convoIndex = 0;
            stay = false;
            Cursor.visible = false;
            me.deactivate();
            gun_controller.set_fire(true);
            anim.set_talk(false);
            cam_controll.set_rotate(true);
            cam_controll2.set_rotate(true);
        }
    }

    void OnGUI()
    {
        if (stay)
        {
            Cursor.visible = true;
            GUI.skin.button.fontSize = 20;
            GUILayout.BeginArea(new Rect(Screen.width * 4 / 10, Screen.height * 0 / 7, Screen.width, Screen.height));
            if (GUILayout.Button("Talk", GUILayout.Width(200), GUILayout.Height(100)))
            {
                convoIndex++;
                if (convoIndex >= conversation.Length)
                {
                    me.activate(quest);
                    convoIndex = conversation.Length - 1;
                }
            }
            GUILayout.EndArea();
            currentTalk = conversation[convoIndex];

            GUI.Label(new Rect(Screen.width * (20 / 100f), Screen.height * (12 / 100f), backk.width * Screen.width/75, backk.height * Screen.height /1000), backk);
            GUI.Label(new Rect(Screen.width * (40 / 100f), Screen.height * (25 / 100f), 0, 20), currentTalk, fontsize);
        }
       
    }

    public bool get_stay()
    {
        return stay;
    }
}