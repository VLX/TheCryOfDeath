﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTransfer : MonoBehaviour {
    public string stage;

    // transfer player from village to wild when he reach the exit
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            Application.LoadLevel(stage);
        PlayerPrefs.SetFloat("PlayerX", 0);
        PlayerPrefs.SetFloat("PlayerY", 0);
        PlayerPrefs.SetFloat("PlayerZ", 0);
    }
}
