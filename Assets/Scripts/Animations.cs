﻿using UnityEngine;
using System.Collections;

public class Animations : MonoBehaviour
{
    public GameObject camera;
    public Quaternion rotacja = Quaternion.identity;
    public Rigidbody move;

    Animator anim;

    public Camera rot_cam, main_cam;

    public GameObject playerGO;
    private InGameMenu menu;
    private bool talk = false;
    private bool moving;
    private Rigidbody rb;

    void Start()
    {
        anim = GetComponent<Animator>();
        menu = playerGO.GetComponent<InGameMenu>();
        rb = playerGO.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire3") && !menu.get_active() && !talk)
        {
            camera.active = false;
            main_cam.enabled = false;
            rot_cam.enabled = true;
        }
        if (Input.GetButtonUp("Fire3"))
        {
            camera.active = true;
            main_cam.enabled = true;
            rot_cam.enabled = false;
        }

        moving = false;


        // animation when player running
        if (Input.GetKey("f") && !menu.get_active() && !talk)
        {
            anim.SetBool("isSprint", true);
            move.velocity = camera.transform.forward * 45;
            anim.Play("Sprint");
            moving = true;
        }
        else
        {
            if (Input.GetButton("Fire1") && !menu.get_active() && !talk)
            {
                anim.SetBool("isShooting", true);
                anim.Play("ShotgunAimIdle");
                moving = true;
            }
            else
            {
                // animation when player walking forward
                if (Input.GetKey("w") && !menu.get_active())
                {
                    anim.SetBool("isWalking", true);
                    move.velocity = camera.transform.forward * 15;
                    anim.Play("WalkFWD");
                    moving = true;
                }
                else
                {
                    // animation when player walking backward
                    if (Input.GetKey("s") && !menu.get_active())
                    {
                        anim.SetBool("back", true);
                        move.velocity = new Vector3(-camera.transform.forward.x, 0, -camera.transform.forward.z) * 10;
                        anim.Play("Back");
                        moving = true;
                    }
                    else
                    {


                        // animation when player walking left
                        if (Input.GetKey("a") && !menu.get_active())
                        {
                            anim.SetBool("Left", true);
                            move.velocity = camera.transform.right * -15;
                            anim.Play("Left");
                            moving = true;
                        }
                        else
                        {

                            // animation when player walking right
                            if (Input.GetKey("d") && !menu.get_active())
                            {
                                anim.SetBool("Right", true);
                                move.velocity = camera.transform.right * 15;
                                anim.Play("Right");
                                moving = true;
                            }
                        }
                    }

                }
            }
        }

        // animation when player crouch
        if (Input.GetKey("c") && !menu.get_active())
        {
            anim.SetBool("isCrouch", true);
            anim.Play("Crouch");
            moving = true;
        }

        if (!moving)
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
         
            
                anim.Play("Start");
            
               
        }

        // rotates player with camera
        rotacja.eulerAngles = new Vector3(
            transform.rotation.eulerAngles.x,
            camera.transform.rotation.eulerAngles.y,
            transform.rotation.eulerAngles.z);
        transform.rotation = rotacja;
    }

    public void set_talk(bool t)
    {
        talk = t;
    }
}