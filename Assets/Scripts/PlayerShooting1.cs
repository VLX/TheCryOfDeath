﻿using UnityEngine;
using System.Collections;

public class PlayerShooting1 : MonoBehaviour
{
    // enemy shooting

    public GameObject shot;
    public Transform shotspawn;

    public AudioClip bam;

    private float nextfire;
    private float firerate = 0.01f;

    void Start (){ StartCoroutine(Shoot()); }

    // enemy shoots automatic every 1.5 seconds
    IEnumerator Shoot()
    {
        while (true)
        {
            Instantiate(shot, shotspawn.position, shotspawn.rotation);
            GetComponent<AudioSource>().PlayOneShot(bam, 0.7F);
            yield return new WaitForSeconds(1.5f);
        }
    }
}
