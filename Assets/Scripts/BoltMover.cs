﻿using UnityEngine;
using System.Collections;

public class BoltMover : MonoBehaviour {
    public float speed;
    public GameObject bullet;
	
	void Start () {
        GetComponent<Rigidbody>().velocity = transform.forward * speed; // the bullet have a starting speed
        Destroy(bullet, 2); // destroys bullet in 2 second (VRAM economy)
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag != "Bullet")
       Destroy(bullet); // destroys bullet if hit anywhere
    }
}
