﻿using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour
{
    private bool loading = true;
    public Texture loadingTexture;

    void Update()
    {
        // appears only if the game loading a scene
        if (Application.isLoadingLevel)
            loading = true;
        else
            loading = false;
    }

    void OnGUI()
    {
        // if the game loading a scene then a texture will appear until the game fully loads the scene 
        if (loading)
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), loadingTexture, ScaleMode.StretchToFill);
    }
}