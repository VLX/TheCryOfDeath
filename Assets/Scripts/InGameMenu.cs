﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenu : MonoBehaviour {

    public bool active,guns, graphics;
    public Terrain terrain;

    public GameObject playerGO,Dante,camer, camer2;
    private PlayerController pc;
    private PlayerShooting gun_controller;
    private CameraFollow camera_controller, camera_controller2;

    public Texture button_image;

    private int shotgun, sniper, sunglasses;

    public GameObject sungl_act;

    // Use this for initialization
    void Start () {
        active = false;
        guns = false;
        graphics = false;
        pc = playerGO.GetComponent<PlayerController>();
        gun_controller = Dante.GetComponent<PlayerShooting>();
        camera_controller = camer.GetComponent<CameraFollow>();
        camera_controller2 = camer2.GetComponent<CameraFollow>();
        shotgun = PlayerPrefs.GetInt("shotgun");
        sniper = PlayerPrefs.GetInt("sniper");
        sunglasses = PlayerPrefs.GetInt("sunglasses");
    }

    void Update()
    {
        if (Input.GetKeyDown("p"))
        {
            if (active)
            {
                graphics = false;
                guns = false;
                active = false;
                gun_controller.set_fire(true);
                camera_controller.set_rotate(true);
                camera_controller2.set_rotate(true);
                Cursor.visible = false;
            }
            else
            {
                active = true;
                gun_controller.set_fire(false);
                camera_controller.set_rotate(false);
                camera_controller2.set_rotate(false);
            }
        }
        
        if(sunglasses == 1)
        {
            sungl_act.active = true;
        }
    }

    // Update is called once per frame
    void OnGUI () {

        if (GUI.Button(new Rect(Screen.width * (90 / 100f), Screen.height * (2 / 100f), Screen.width * (8 / 100f), Screen.height * (8 / 100f)), button_image))
        {
            if (active)
            {
                graphics = false;
                guns = false;
                active = false;
                gun_controller.set_fire(true);
                camera_controller.set_rotate(true);
                camera_controller2.set_rotate(true);
                Cursor.visible = false;
            }
            else
            {
                active = true;
                gun_controller.set_fire(false);
                camera_controller.set_rotate(false);
                camera_controller2.set_rotate(false);
            }
            
        }
        if (active)
        {
            
            Cursor.visible = true;

            // main
            if (GUI.Button(new Rect(Screen.width * (90 / 100f), Screen.height * (10 / 100f), Screen.width * (8 / 100f), Screen.height * (5 / 100f)), "Guns"))
            {
                guns = true;
                graphics = false;
            }
            if (GUI.Button(new Rect(Screen.width * (90 / 100f), Screen.height * (15 / 100f), Screen.width * (8 / 100f), Screen.height * (5 / 100f)), "Graphics"))
            {
                graphics = true;
                guns = false;
            }

            if (GUI.Button(new Rect(Screen.width * (90 / 100f), Screen.height * (20 / 100f), Screen.width * (8 / 100f), Screen.height * (5 / 100f)), "Save"))
            {
                //Saving
                PlayerPrefs.SetFloat("PlayerX", pc.transform.position.x);
                PlayerPrefs.SetFloat("PlayerY", pc.transform.position.y);
                PlayerPrefs.SetFloat("PlayerZ", pc.transform.position.z);
                PlayerPrefs.SetString("Scene", SceneManager.GetActiveScene().name);
                active = false;
                gun_controller.set_fire(true);
                Cursor.visible = false;
            }
            if (GUI.Button(new Rect(Screen.width * (90 / 100f), Screen.height * (25 / 100f), Screen.width * (8 / 100f), Screen.height * (5 / 100f)), "Exit Game"))
            {
                Application.Quit();
                active = false;
                gun_controller.set_fire(true);
                Cursor.visible = false;
            }

            // guns
            if (guns) {
               if (GUI.Button(new Rect(Screen.width * (78 / 100f), Screen.height * (5 / 100f), Screen.width * (10 / 100f), Screen.height * (5 / 100f)), "Machine Gun"))
                {
                    gun_controller.set_active_ak();
                guns = false;
                Cursor.visible = false;
                }
                if (shotgun == 1)
                {
                    if (GUI.Button(new Rect(Screen.width * (78 / 100f), Screen.height * (10 / 100f), Screen.width * (10 / 100f), Screen.height * (5 / 100f)), "Shotgun"))
                    {
                        gun_controller.set_active_spas();
                        guns = false;
                        Cursor.visible = false;
                    }
                }
                if (sniper == 1)
                {
                    if (GUI.Button(new Rect(Screen.width * (78 / 100f), Screen.height * (15 / 100f), Screen.width * (10 / 100f), Screen.height * (5 / 100f)), "Sniper"))
                    {
                        gun_controller.set_active_sniper();
                        guns = false;
                        Cursor.visible = false;
                    }
                }
            }

            // graphics
            if (graphics)
            {
                if (GUI.Button(new Rect(Screen.width * (80 / 100f), Screen.height * (5 / 100f), Screen.width * (8 / 100f), Screen.height * (5 / 100f)), "Very Low"))
                {
                    terrain.heightmapPixelError = 200;
                    terrain.basemapDistance = 0;
                    graphics = false;
                    Cursor.visible = false;
                }
                if (GUI.Button(new Rect(Screen.width * (80 / 100f), Screen.height * (10 / 100f), Screen.width * (8 / 100f), Screen.height * (5 / 100f)), "Low"))
                {
                    terrain.heightmapPixelError = 150;
                    terrain.basemapDistance = 500;
                    graphics = false;
                    Cursor.visible = false;
                }
                if (GUI.Button(new Rect(Screen.width * (80 / 100f), Screen.height * (15 / 100f), Screen.width * (8 / 100f), Screen.height * (5 / 100f)), "Medium"))
                {
                    terrain.heightmapPixelError = 100;
                    terrain.basemapDistance = 1000;
                    graphics = false;
                    Cursor.visible = false;
                }
                if (GUI.Button(new Rect(Screen.width * (80 / 100f), Screen.height * (20 / 100f), Screen.width * (8 / 100f), Screen.height * (5 / 100f)), "High"))
                {
                    terrain.heightmapPixelError = 50;
                    terrain.basemapDistance = 1500;
                    graphics = false;
                    Cursor.visible = false;
                }
                if (GUI.Button(new Rect(Screen.width * (80 / 100f), Screen.height * (25 / 100f), Screen.width * (8 / 100f), Screen.height * (5 / 100f)), "Very High"))
                {
                    terrain.heightmapPixelError = 0;
                    terrain.basemapDistance = 2000;
                    graphics = false;
                    Cursor.visible = false;
                }
            }
        }	
	}

    public void set_shotgun()
    {
        PlayerPrefs.SetInt("shotgun", 1);
        shotgun = 1;
    }

    public void set_sniper()
    {
        PlayerPrefs.SetInt("sniper", 1);
        sniper = 1;
    }

    public void set_sunglasses()
    {
        PlayerPrefs.SetInt("sunglasses", 1);
        sunglasses = 1;
    }

    public bool get_active()
    {
        return active;
    }
}
