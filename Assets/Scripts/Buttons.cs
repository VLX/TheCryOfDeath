﻿using UnityEngine;
using System.Collections;
using System;

public class Buttons : MonoBehaviour {
    // Start page buttons

    public Canvas canvas;
    private String scene;

    private void Start()
    {
       // PlayerPrefs.DeleteAll();
    }

    void OnGUI()
    {
        Cursor.visible = true;

        canvas.worldCamera = Camera.main;
        GUI.skin.button.fontSize = 20;
        GUILayout.BeginArea(new Rect(Screen.width*2 / 7, Screen.height * 2 / 7, Screen.width, Screen.height));

        if (GUILayout.Button("Start", GUILayout.Width(500), GUILayout.Height(100)))
        {         
            scene = PlayerPrefs.GetString("Scene");

            if (scene != "")
                Application.LoadLevel(scene);
            else
                Application.LoadLevel("0");

            Destroy(this);
        }
        if (GUILayout.Button("Delete Saved Data", GUILayout.Width(500), GUILayout.Height(100)))
        {
            PlayerPrefs.DeleteAll();
        }
        
        if (GUILayout.Button("Quit", GUILayout.Width(500), GUILayout.Height(100)))
        {
            Destroy(this);
            Application.Quit();
        }
        GUILayout.EndArea();
    }
}