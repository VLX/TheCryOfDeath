﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Quests : MonoBehaviour {

   private int q1, q2, q3, q4, q5;
   private int cntq1, cntq2, cntq3, cntq4, cntq5;

    public GameObject playerGO;
    private PlayerController pc;
    private InGameMenu gun_active;


    void Start () {
        q1 = PlayerPrefs.GetInt("q1");
        q2 = PlayerPrefs.GetInt("q2");
        q3 = PlayerPrefs.GetInt("q3");
        q4 = PlayerPrefs.GetInt("q4");
        q5 = PlayerPrefs.GetInt("q5");
        pc = playerGO.GetComponent<PlayerController>();
        gun_active = playerGO.GetComponent<InGameMenu>();
        cntq1 = PlayerPrefs.GetInt("cntq1");
        cntq2 = PlayerPrefs.GetInt("cntq2");
        cntq3 = PlayerPrefs.GetInt("cntq3");
        cntq4 = PlayerPrefs.GetInt("cntq4");
        cntq5 = PlayerPrefs.GetInt("cntq5");
    }
	
	void Update () {
        if (cntq1 >= 2)
        {
            cntq1 = 0;
            q1 = 0;
            PlayerPrefs.SetInt("q1", 0);
            PlayerPrefs.SetInt("cntq1", 0);
            pc.addXP(200);
        }
        if (cntq2 >= 3)
        {
            cntq2 = 0;
            q2 = 0;
            PlayerPrefs.SetInt("q2", 0);
            PlayerPrefs.SetInt("cntq2", 0);
            pc.addXP(1000);
        }
        if (cntq3 >= 4)
        {
            cntq3 = 0;
            q3 = 0;
            PlayerPrefs.SetInt("q3", 0);
            PlayerPrefs.SetInt("cntq3", 0);
            pc.addXP(4000);
            gun_active.set_shotgun();
        }
        if (cntq4 >= 4)
        {
            cntq4 = 0;
            q4 = 0;
            PlayerPrefs.SetInt("q4", 0);
            PlayerPrefs.SetInt("cntq4", 0);
            pc.addXP(10000);
            gun_active.set_sniper();
        }
        if (cntq5 >= 1)
        {
            cntq5 = 0;
            q5 = 0;
            PlayerPrefs.SetInt("q5", 0);
            PlayerPrefs.SetInt("cntq5", 0);
            pc.addXP(40000);
            gun_active.set_sunglasses();
        }
    }

    public int getq1()
    {
        return q1;
    }

    public int getq2()
    {
        return q2;
    }

    public int getq3()
    {
        return q3;
    }

    public int getq4()
    {
        return q4;
    }

    public int getq5()
    {
        return q5;
    }

    public void setq1()
    {
        q1 = 1;
        PlayerPrefs.SetInt("q1",1);
    }

    public void setq2()
    {
        q2 = 1;
        PlayerPrefs.SetInt("q2", 1);
    }

    public void setq3()
    {
        q3 = 1;
        PlayerPrefs.SetInt("q3", 1);
    }

    public void setq4()
    {
        q4 = 1;
        PlayerPrefs.SetInt("q4", 1);
    }

    public void setq5()
    {
        q5 = 1;
        PlayerPrefs.SetInt("q5", 1);
    }

    public void setcntq1()
    {
        cntq1++;
        PlayerPrefs.SetInt("cntq1", cntq1);
    }

    public void setcntq2()
    {
        cntq2++;
        PlayerPrefs.SetInt("cntq2", cntq2);
    }

    public void setcntq3()
    {
        cntq3++;
        PlayerPrefs.SetInt("cntq3", cntq3);
    }

    public void setcntq4()
    {
        cntq4++;
        PlayerPrefs.SetInt("cntq4", cntq4);
    }

    public void setcntq5()
    {
        cntq5++;
        PlayerPrefs.SetInt("cntq5", cntq5);
    }

    public int getcntq1()
    {
        return cntq1;  
    }

    public int getcntq2()
    {
        return cntq2;
    }

    public int getcntq3()
    {
        return cntq3;
    }

    public int getcntq4()
    {
        return cntq4;
    }

    public int getcntq5()
    {
        return cntq5;
    }
}
