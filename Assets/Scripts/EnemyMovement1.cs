﻿using UnityEngine;
using System.Collections;

public class EnemyMovement1 : MonoBehaviour
{
    // this script is only for final boss

    // follow player
    public Transform player,enemy;
    UnityEngine.AI.NavMeshAgent nav;
    private float distance;

    // health
    public TextMesh healthText;
    private float health = 100;
    public GameObject playerGO, Dante;
    private PlayerController pc;
    private PlayerShooting shooting;

    public int enemyXP;
    public int enemyLevel;

    // unlock next stage
    public bool stageking;

    private bool hit = false;

    private Quests questController;

    Animator anim;

    void Awake ()
    {
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
        pc = playerGO.GetComponent<PlayerController>();
        questController = playerGO.GetComponent<Quests>();
        shooting = Dante.GetComponent<PlayerShooting>();
        anim = GetComponent<Animator>();
    }

    void Update ()
    {
        // follows player
        nav.SetDestination(player.position);  
    }

    void OnTriggerEnter(Collider other)
    {
        // if hit by bullet
        if (other.tag == "Bullet")
        {
            // erase enemy's health with type 6*player level/enemy level
            if (shooting.get_active_ak())
                health -= 6 * pc.getLevel() / enemyLevel;

            if (shooting.get_active_spas())
                health -= 20 * pc.getLevel() / enemyLevel;

            if (shooting.get_active_sniper())
                health -= 50 * pc.getLevel() / enemyLevel;

            if (shooting.get_active_sniper2())
                health -= 50 * pc.getLevel() / enemyLevel;

            // enemy dies
            if (health <= 0)
            {
                // when the stage king dies, the gate of next stage must open
                if (stageking){pc.opencave();}

                Destroy(this.gameObject);
                // adds the amount of xp points that player must recieve
                pc.addXP(enemyXP);

                if (questController.getq5() == 1)
                {
                    questController.setcntq5();
                }
            }

            // prints the new value after the enemy hit by bullet
            healthText.text = "" + health;
        }
        // if enemy reach the player 
        if (other.gameObject.tag == "Player")
        {

            // erase player health with type enemylevel*0.22/playerlevel
            pc.eraseHealth(enemyLevel * 0.22f / pc.getLevel());
        }
    }
}
