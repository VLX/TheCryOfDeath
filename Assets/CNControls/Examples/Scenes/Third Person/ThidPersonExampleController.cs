﻿using UnityEngine;
using CnControls;

// This is merely an example, it's for an example purpose only
// Your game WILL require a custom controller scripts, there's just no generic character control systems, 
// they at least depend on the animations

[RequireComponent(typeof(CharacterController))]
public class ThidPersonExampleController : MonoBehaviour
{
    public float MovementSpeed = 10f;
    public GameObject camera;

    private Transform _mainCameraTransform;
    private Transform _transform;
    private CharacterController _characterController;
    private Vector3 movement;

    private Rigidbody rb;

    private void OnEnable()
    {
        _mainCameraTransform = Camera.main.GetComponent<Transform>();
        _characterController = GetComponent<CharacterController>();
        _transform = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        // move
        float moveHorizontal = CnInputManager.GetAxis("Horizontal");
        float moveVertical = CnInputManager.GetAxis("Vertical");
        movement = new Vector3(moveHorizontal * 2, 0, moveVertical * 2);

        // moves the player based on camera rotation
        //rb.AddForce(moveVertical * 2 * camera.transform.forward * 15);
        //rb.AddForce(moveHorizontal * 2 * camera.transform.right * 15);
    }


    //public void Update()
    //{
    //    // Just use CnInputManager. instead of Input. and you're good to go
    //    var inputVector = new Vector3(CnInputManager.GetAxis("Horizontal"), CnInputManager.GetAxis("Vertical"));
    //    Vector3 movementVector = Vector3.zero;

    //   // float moveHorizontalm = CnInputManager.GetAxis("Horizontal");
    //   // float moveVerticalm = CnInputManager.GetAxis("Vertical");
    //   // movement = new Vector3(moveHorizontalm * 2, 0, moveVerticalm * 2);

    //    // If we have some input
    //    if (inputVector.sqrMagnitude > 0.001f)
    //    {
    //        movementVector = _mainCameraTransform.TransformDirection(inputVector);
    //        movementVector.y = 0f;
    //        movementVector.Normalize();
    //        _transform.forward = movementVector;
    //    }

    //    movementVector += Physics.gravity;
    //    _characterController.Move(movementVector * 10 * Time.deltaTime);
    //   // _characterController.Move(movementVector * camera.transform.right * Time.deltaTime);
    //    // rb.AddForce(moveVertical * 2 * camera.transform.forward * speed);
    //    // rb.AddForce(moveHorizontal * 2 * camera.transform.right * speed);
    //}
}
